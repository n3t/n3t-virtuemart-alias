<?php
/**
 * @package n3t Virtuemart Alias
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2016-2017 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgSystemN3tVmAlias extends JPlugin {

	public function onAfterRoute()
	{
  	$app = JFactory::getApplication();	
    $input = $app->input;
    if ($input->getCmd('option') == 'com_virtuemart') {
      switch ($input->getCmd('view')) {
        case 'product':
          if ($input->getCmd('task') == 'apply' || $input->getCmd('task') == 'save') 
            $this->generateProductSlug($input);
          elseif ($input->getCmd('task') == 'edit' || $input->getCmd('task') == 'add')
            $this->disableSlugInput();
          break;
        case 'category':
        case 'manufacturer':
        case 'shipmentmethod':
        case 'paymentmethod':
          if ($input->getCmd('task') == 'apply' || $input->getCmd('task') == 'save') {
            $fieldNames = array(
              'category' => 'category_name',
              'manufacturer' => 'mf_name',
              'shipmentmethod' => 'shipment_name',
              'paymentmethod' => 'payment_name',
            );
            $this->generateSimpleSlug($input, $fieldNames[$input->getCmd('view')]);
          } elseif ($input->getCmd('task') == 'edit' || $input->getCmd('task') == 'add')
            $this->disableSlugInput();
          break;  
      }
    }
	}
  
  protected function generateProductSlug($input)
  {
    if ($input->get('slug') 
    && $this->params->get('gen_type', 'empty') == 'empty')
      return;
      
    $slug = array();
          
    if ($this->params->get('pos_name', 1))   
      $slug[(int)$this->params->get('pos_name', 1)] = $input->getStr('product_name');
      
    if ($this->params->get('pos_sku', 0))
      $slug[(int)$this->params->get('pos_sku', 0)] = $input->getStr('product_sku');
      
    if ($this->params->get('pos_manufacturer', 0)) {  
      $manufacturer = $input->get('virtuemart_manufacturer_id',array(),'ARRAY'); 
      if (is_array($manufacturer) && count($manufacturer) && (int)$manufacturer[0]) {
        $manufacturer = (int)$manufacturer[0];
        $db = JFactory::getDBO();
        $db->setQuery('select mf_name from #__virtuemart_manufacturers_cs_cz where virtuemart_manufacturer_id='.$manufacturer);        
        $manufacturer = $db->loadResult();
        if ($manufacturer)
          $slug[(int)$this->params->get('pos_manufacturer', 0)] = $manufacturer;
      }            
    }
    
    if ($this->params->get('pos_gtin', 0))
      $slug[(int)$this->params->get('pos_gtin', 0)] = $input->getStr('product_gtin');
      
    if ($this->params->get('pos_mpn', 0))
      $slug[(int)$this->params->get('pos_mpn', 0)] = $input->getStr('product_mpn');
      
    if ($this->params->get('pos_custom', 0))
      $slug[(int)$this->params->get('pos_custom', 0)] = $this->params->get('custom_text', '');

    ksort($slug);  
    $slug = implode(' ',$slug);
    $slug = JApplicationHelper::stringURLSafe($slug, $input->getStr('vmlang', ''));
                     
    $input->set('slug',$slug);      
  }
    
  protected function generateSimpleSlug($input, $nameField)
  {
    if ($input->get('slug')
    && $this->params->get('gen_type', 'empty') == 'empty')
      return;

    $slug = JApplicationHelper::stringURLSafe($input->getStr($nameField, ''), $input->getStr('vmlang', ''));

    $input->set('slug', $slug);
    // Payment and Shipment method reads data directly from $_POST
    $_POST['slug'] = $slug;
  }

  protected function disableSlugInput()
  {
    if ($this->params->get('gen_type', 'empty') == 'always') {
      $doc = JFactory::getDocument();
      $this->loadLanguage();
      JText::script('PLG_SYSTEM_N3TVMALIAS_AUTO_GENERATED');
      JHtml::_('jquery.framework');
      $doc->addScriptDeclaration("
        jQuery(function() {
          jQuery('input[name=slug]').prop('readonly', true).prop('title', Joomla.JText._('PLG_SYSTEM_N3TVMALIAS_AUTO_GENERATED'));
        });        
      ");        
    }
  }
}