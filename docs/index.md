n3t Virtuemart Alias
====================

n3t Virtuemart Alias Joomla! plugin giving user better control, on how products alias are generated.

Installation
------------

n3t Virtuemart Alias is Joomla! system plugin. It could be installed as any other extension in
Joomla!

After installing **do not forget to enable the plugin** from Plugin Manager in your
Joomla! installation.